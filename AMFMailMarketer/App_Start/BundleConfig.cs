﻿using System.Web;
using System.Web.Optimization;

namespace AMFMailMarketer
{
    public class BundleConfig
    {
        // Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/dashboard").Include(
                        "~/Scripts/amcharts.js",
                        "~/Scripts/serial.js",
                        "~/Scripts/light.js",
                        "~/Scripts/radar.js",                        
                        "~/Scripts/css3clock.js",
                        "~/Scripts/skycons.js"));
            bundles.Add(new ScriptBundle("~/bundles/protovix").Include(
                        "~/Scripts/protovis.js",
                        "~/Scripts/vix.js"));

            bundles.Add(new ScriptBundle("~/bundles/fabochart").Include(
                        "~/Scripts/fabochart.js",
                        "~/Scripts/Chart.js"));

            bundles.Add(new ScriptBundle("~/bundles/fabochart").Include(
                        "~/Scripts/fabochart.js",
                        "~/Scripts/Chart.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/nicescroll").Include(
                        "~/Scripts/jquery.nicescroll.js"));

            bundles.Add(new ScriptBundle("~/bundles/vroom").Include(
                        "~/Scripts/vroom.js",
                        "~/Scripts/TweenLite.js",
                        "~/Scripts/CSSPlugin.js"));

            bundles.Add(new ScriptBundle("~/bundles/menuscript").Include(
                        "~/Scripts/scripts.js"));
            ///Hojas de Estilo
 
            bundles.Add(new StyleBundle("~/Content/Login").Include(
                "~/Content/bootstrap.css",
                "~/Content/style.css",
                "~/Content/font-awesome.css",
                "~/Content/icon-font.css"));

            bundles.Add(new StyleBundle("~/Content/fabochart").Include(
                "~/Content/bars.css",
                "~/Content/fabochart.css"));

            bundles.Add(new StyleBundle("~/Content/vroom").Include(
                "~/Content/vroom.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}